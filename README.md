# database

作为 ORM 跨数据库类型的接口约束, 在官方接口下扩展独立两个接口

```
package database

import std.collection.HashMap
import std.time.DateTime

// 用户侧数据接口
public interface SqlData {
    func fromSqlData(row: SqlRowData): Unit
    func toSqlData(): HashMap<String, Any>
}
// 各种数据库驱动需要实现的数据接口
// 根据反射或生成 class、struct 填充数据
public interface SqlRowData {
    func getInt(name: String): ?Int64
    func getInt16(name: String): ?Int16
    func getInt32(name: String): ?Int32
    func getInt64(name: String): ?Int64
    func getUInt16(name: String): ?UInt16
    func getUInt32(name: String): ?UInt32
    func getUInt64(name: String): ?UInt64
    func getFloat32(name: String): ?Float32
    func getFloat64(name: String): ?Float64
    func getBool(name: String): ?Bool
    func getString(name: String): ?String
    func getBinary(name: String): ?Array<Byte>
    func getTime(name: String): ?DateTime
}
```
